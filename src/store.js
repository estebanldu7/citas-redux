import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import mainReducer from './reducers'

const middleware = [thunk];

//Add local storage
const storageState = localStorage.getItem('meetings') ? JSON.parse(localStorage.getItem('meetings')) : [];

const store = createStore(mainReducer, storageState, compose(applyMiddleware(...middleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
));

export default store;
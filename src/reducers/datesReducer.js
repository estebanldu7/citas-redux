import {SHOW_DATES, ADD_DATE, DELETE_DATE} from "../actions/types";

//state initial, every reducer has to has owner reducer

const initialState = {
    meeting : []
};

export default function (state = initialState, action) {
    switch (action.type){
        case SHOW_DATES:
            return{
                ...state
            };
        case ADD_DATE:
            return{
                ...state,
                meeting : [...state.meeting, action.payload]
            };
        case DELETE_DATE:
            return{
                ...state,
                meeting: state.meeting.filter(meeting => meeting.id !== action.payload)
            };
        default:
            return state
    }
}
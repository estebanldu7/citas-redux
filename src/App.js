import React, { Component } from 'react';
import Header from "./components/Header";
import AgregarCita from "./components/AgregarCita";
import ListaCita from "./components/ListaCita";

//Redux
import {Provider} from 'react-redux';
import store from './store';

class App extends Component {

  render() {
    return (
        <Provider store={store}>
            <div className="App">
                <Header titulo={'Administador de Pacientes de Veterinaria.'}/>
                <div className={"row"}>
                    <div className={"col-md-6"}>
                        <AgregarCita/>
                    </div>
                    <div className={"col-md-6"}>
                        <ListaCita/>
                    </div>
                </div>
            </div>
        </Provider>
    );
  }
}

export default App;

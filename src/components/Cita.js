import  React from 'react'

//Redux
import {connect} from 'react-redux';
import {deleteDate} from "../actions/datesActions";

class Cita extends React.Component{

    deleteMeeting = () => {
        this.props.deleteDate(this.props.info.id) //Call from ListaCita.js
    };

    render(){
        const {fecha, mascota, propietario, sintomas, id} = this.props.info;

        return(
            <div className={"media mt-3"}>
                <div className={"media-body"}>
                    <h3 className={"mt-0"}>{mascota}</h3>
                    <p className={"card-text"}><span>Nombre del dueño:</span>{propietario}</p>
                    <p className={"card-text"}><span>Fecha: </span> {fecha}</p>
                    <p className={"card-text"}><span>Sintomas: </span>{sintomas}</p>
                    <p className={"card-text"}></p>
                    <button onClick={this.deleteMeeting} className={"btn btn-danger"}>Borrar</button>
                </div>
            </div>
        )
    }
}

export default connect(null, {deleteDate}) (Cita)
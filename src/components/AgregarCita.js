import  React from 'react';
import uuid from 'uuid';

//Redux
import {connect} from 'react-redux';
import {addDate} from "../actions/datesActions";
import {showError} from "../actions/errorActions";

class AgregarCita extends React.Component{

    componentWillMount(){
        this.props.showError(false)
    }

    nombreMascota =  React.createRef();
    nombrePropietario = React.createRef();
    fecha = React.createRef();
    hora = React.createRef();
    sintomas = React.createRef();

    bookMeeting = (e) => {
        e.preventDefault();

        //Get value from field components
        const mascota = this.nombreMascota.current.value,
              propietario = this.nombrePropietario.current.value,
              fecha = this.fecha.current.value,
              hora = this.hora.current.value,
              sintomas = this.sintomas.current.value;

        if(mascota === '' || propietario === '' || fecha === '' || sintomas === ''){
           this.props.showError(true);
        }else{
            //Create a object with data information
            const newMeeting = {
                id: uuid(),
                mascota,
                propietario,
                fecha,
                hora,
                sintomas
            };

            this.props.addDate(newMeeting); //Send info to App.js (Parent)

            //Reset or clear form
            e.currentTarget.reset();
            this.props.showError(false);
        }
    };

    render(){

        const existeError = this.props.error;
        console.log(existeError)

        return(
            <div className={"card mt-5"}>
                <div className={"card-body"}>
                    <h2 className={"card-title text-center mb-5"}> Agregar citas aqui</h2>

                    <form onSubmit={this.bookMeeting}>
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-label">Nombre Mascota</label>
                            <div className="col-sm-8 col-lg-10">
                                <input type="text" ref={this.nombreMascota} className="form-control" placeholder="Nombre Mascota" />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-label">Nombre Dueño</label>
                            <div className="col-sm-8 col-lg-10">
                                <input type="text" ref={this.nombrePropietario} className="form-control"  placeholder="Nombre Dueño de la Mascota" />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-label">Fecha</label>
                            <div className="col-sm-8 col-lg-4  mb-4 mb-lg-0">
                                <input type="date" ref={this.fecha} className="form-control" />
                            </div>

                            <label className="col-sm-4 col-lg-2 col-form-label">Hora</label>
                            <div className="col-sm-8 col-lg-4">
                                <input type="time" ref={this.hora} className="form-control" />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-label">Sintomas</label>
                            <div className="col-sm-8 col-lg-10">
                                <textarea ref={this.sintomas}  className="form-control"></textarea>
                            </div>
                        </div>
                        <div className="form-group row justify-content-end">
                            <div className="col-sm-3">
                                <button type="submit" className="btn btn-success w-100">Agregar</button>
                            </div>
                        </div>
                    </form>
                    {existeError? <div className={"alert alert-danger text-center"}>Todos los campos son obligatorios.</div> : ''}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    meeting: state.meeting.meeting,
    error: state.error.error
});

export default connect(mapStateToProps, {addDate, showError}) (AgregarCita);
import  React from 'react';
import Cita from "./Cita";

//Redux
import {connect} from 'react-redux';
import {getDates} from "../actions/datesActions";
import store from '../store';

store.subscribe(() => {
    localStorage.setItem('meetings', JSON.stringify(store.getState()))
});

class ListaCita extends React.Component{

    componentDidMount(){
        this.props.getDates();
    }

    render(){
        const meetings = this.props.meeting;
        const message = Object.keys(meetings).length === 0 ? 'No hay citas' : 'Administrar Tus Citas Aqui'; // If exist meetings

        return(
            <div className={"card mt-5"}>
                <div className={"card-body"}>
                    <h2 className={"card-title text-center"}>
                        {message}
                    </h2>
                    <div className={"lista-citas"}>
                        {Object.keys(this.props.meeting).map(meeting =>(
                            <Cita
                                key = {meeting}
                                info = {this.props.meeting[meeting]}
                            />
                        ))}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    meeting: state.meeting.meeting
});

export default connect(mapStateToProps, {getDates}) (ListaCita);